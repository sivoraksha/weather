package com.weather.application.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.weather.application.helper.deserializers.AccuWeatherForecastDeserializer;

@JsonDeserialize(using = AccuWeatherForecastDeserializer.class)
public class AccuWeatherForecastDto
{
  private String precipitation;
  private Float windSpeed;
  private String windDirection;
  private Float windGust;
  private Integer maxTemperature;
  private Integer minTemperature;
  private Float realFeelTemperature;
  private Integer stormProbability;

  public AccuWeatherForecastDto()
  {
  }

  public AccuWeatherForecastDto(
      String precipitation,
      Float windSpeed,
      String windDirection,
      Float windGust,
      Integer maxTemperature,
      Integer minTemperature,
      Float realFeelTemperature, Integer stormProbability)
  {
    this.precipitation = precipitation;
    this.windSpeed = windSpeed;
    this.windDirection = windDirection;
    this.windGust = windGust;
    this.maxTemperature = maxTemperature;
    this.minTemperature = minTemperature;
    this.realFeelTemperature = realFeelTemperature;
    this.stormProbability = stormProbability;
  }

  public String getPrecipitation()
  {
    return precipitation;
  }

  public void setPrecipitation(String precipitation)
  {
    this.precipitation = precipitation;
  }

  public Float getWindSpeed()
  {
    return windSpeed;
  }

  public void setWindSpeed(Float windSpeed)
  {
    this.windSpeed = windSpeed;
  }

  public String getWindDirection()
  {
    return windDirection;
  }

  public void setWindDirection(String windDirection)
  {
    this.windDirection = windDirection;
  }

  public Float getWindGust()
  {
    return windGust;
  }

  public void setWindGust(Float windGust)
  {
    this.windGust = windGust;
  }

  public Integer getMaxTemperature()
  {
    return maxTemperature;
  }

  public void setMaxTemperature(Integer maxTemperature)
  {
    this.maxTemperature = maxTemperature;
  }

  public Integer getMinTemperature()
  {
    return minTemperature;
  }

  public void setMinTemperature(Integer minTemperature)
  {
    this.minTemperature = minTemperature;
  }

  public Float getRealFeelTemperature()
  {
    return realFeelTemperature;
  }

  public void setRealFeelTemperature(Float realFeelTemperature)
  {
    this.realFeelTemperature = realFeelTemperature;
  }

  public Integer getStormProbability()
  {
    return stormProbability;
  }

  public void setStormProbability(Integer stormProbability)
  {
    this.stormProbability = stormProbability;
  }
}
