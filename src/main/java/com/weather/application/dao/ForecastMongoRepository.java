package com.weather.application.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.weather.application.model.MeteoprogForecast;

@Repository
public interface ForecastMongoRepository extends MongoRepository<MeteoprogForecast, String>{}
