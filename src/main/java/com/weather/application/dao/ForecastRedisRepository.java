package com.weather.application.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.weather.application.model.AccuWeatherForecast;

@Repository
public interface ForecastRedisRepository extends CrudRepository<AccuWeatherForecast, String>
{
}
