package com.weather.application.helper.deserializers;

import java.io.IOException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.weather.application.dto.AccuWeatherForecastDto;

public class AccuWeatherForecastDeserializer extends StdDeserializer<AccuWeatherForecastDto>
{
  private static final String VALUE_KEY = "Value";
  private static final String DAY_KEY = "Day";
  private static final String TEMPERATURE_KEY = "Temperature";
  private static final String REAL_TEMPERATURE_KEY = "RealFeelTemperature";
  private static final String MAXIMUM_KEY = "Maximum";
  private static final String MINIMUM_KEY = "Minimum";
  private static final String SPEED_KEY = "Speed";
  private static final String WIND_KEY = "Wind";

  public AccuWeatherForecastDeserializer()
  {
    this(null);
  }

  protected AccuWeatherForecastDeserializer(Class<?> vc)
  {
    super(vc);
  }

  @Override
  public AccuWeatherForecastDto deserialize(JsonParser jsonParser, DeserializationContext context)
      throws IOException
  {
    JsonNode node = jsonParser.getCodec().readTree(jsonParser);
    node = node.get("DailyForecasts").get(0);

    String precipitation = node.get(DAY_KEY)
        .get("LongPhrase")
        .asText();

    String windDirection = node.get(DAY_KEY)
        .get(WIND_KEY)
        .get("Direction")
        .get("English")
        .asText();

    float windSpeed = node.get(DAY_KEY)
        .get(WIND_KEY)
        .get(SPEED_KEY)
        .get(VALUE_KEY)
        .floatValue();

    float windGust = node.get(DAY_KEY)
        .get("WindGust")
        .get(SPEED_KEY)
        .get(VALUE_KEY)
        .floatValue();

    int minTemperature = node.get(TEMPERATURE_KEY)
        .get(MINIMUM_KEY)
        .get(VALUE_KEY)
        .intValue();

    int maxTemperature = node.get(TEMPERATURE_KEY)
        .get(MAXIMUM_KEY)
        .get(VALUE_KEY)
        .intValue();

    float maxRealFeelTemperature = node.get(REAL_TEMPERATURE_KEY)
        .get(MAXIMUM_KEY)
        .get(VALUE_KEY)
        .floatValue();

    int stormProbability = node.get(DAY_KEY)
        .get("ThunderstormProbability")
        .intValue();

    return new AccuWeatherForecastDto(precipitation, windSpeed, windDirection, windGust,
        maxTemperature, minTemperature, maxRealFeelTemperature, stormProbability);
  }
}
