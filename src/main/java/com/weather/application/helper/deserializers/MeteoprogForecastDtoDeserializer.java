package com.weather.application.helper.deserializers;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.weather.application.dto.MeteoprogForecastDto;

public class MeteoprogForecastDtoDeserializer extends StdDeserializer<MeteoprogForecastDto>
{
  private static final String WIND_KEY = "wind";
  private static final String TEMPERATURE_KEY = "temperature";
  private static final String NAME_KEY = "name";

  public MeteoprogForecastDtoDeserializer()
  {
    this(null);
  }

  protected MeteoprogForecastDtoDeserializer(Class<?> vc)
  {
    super(vc);
  }

  @Override
  public MeteoprogForecastDto deserialize(JsonParser jsonParser, DeserializationContext context)
      throws IOException
  {
    JsonNode node = jsonParser.getCodec().readTree(jsonParser);

    node = node.get("data")
            .get("weather")
            .get(getFormattedCurrentDate())
            .get("forecast")
            .get("12:00:00");

    String precipitation = node.get("precipitation")
        .get(NAME_KEY)
        .asText();

    String windDiraction = node.get(WIND_KEY)
        .get(NAME_KEY)
        .asText();

    float maxWindSpeed = node.get(WIND_KEY)
        .get("speed_high")
        .floatValue();

    int minTemperature = node.get(TEMPERATURE_KEY)
        .get("low")
        .intValue();

    int maxTemperature = node.get(TEMPERATURE_KEY)
        .get("high")
        .intValue();

    float realFeelTemperature = node.get(TEMPERATURE_KEY)
        .get("feelslike")
        .floatValue();

    int stormProbability = node.get("probability_storm").intValue();

    return new MeteoprogForecastDto(precipitation, maxWindSpeed, windDiraction, 0.0f,
        maxTemperature, minTemperature, realFeelTemperature, stormProbability);
  }
  private String getFormattedCurrentDate()
  {
    LocalDate currentDate = LocalDate.now();
    int daysOfMonth = currentDate.getDayOfMonth();
    String formattedDate = currentDate.format(DateTimeFormatter.ISO_LOCAL_DATE);

    return formattedDate.substring(0, 8).concat(String.valueOf(daysOfMonth));
  }

}
