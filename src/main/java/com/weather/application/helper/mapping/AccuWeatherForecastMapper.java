package com.weather.application.helper.mapping;

import com.weather.application.dto.AccuWeatherForecastDto;
import com.weather.application.model.AccuWeatherForecast;
import org.mapstruct.Mapper;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface AccuWeatherForecastMapper {

    AccuWeatherForecast toAccuWeatherForecast(AccuWeatherForecastDto accuWeatherForecastDto);

    default List<AccuWeatherForecast> toAccuWeatherForecastList(List<AccuWeatherForecastDto> accuWeatherForecastDtos) {
        return accuWeatherForecastDtos.stream()
                .map(this::toAccuWeatherForecast)
                .collect(Collectors.toList());
    }
}
