package com.weather.application.helper.mapping;

import com.weather.application.dto.MeteoprogForecastDto;
import com.weather.application.model.MeteoprogForecast;
import org.mapstruct.Mapper;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface MeteoprogForecastMapper {

    MeteoprogForecast toMeteoprogForecast(MeteoprogForecastDto meteoprogForecastDto);

    default List<MeteoprogForecast> toMeteoprogForecastList(List<MeteoprogForecastDto> meteoprogForecastDto) {
        return meteoprogForecastDto.stream()
                .map(this::toMeteoprogForecast)
                .collect(Collectors.toList());
    }
}
