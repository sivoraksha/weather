package com.weather.application.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.weather.application.dto.AccuWeatherForecastDto;
import com.weather.application.service.api.AccuWeatherForecastIntegrationApi;

@Service
public class AccuWeatherApiService
{
  @Value("${api.accu.weather.key}")
  private String apiKey;

  @Value("${api.accu.weather.query.details}")
  private Boolean fullDetailsResponse;

  @Value("${api.accu.weather.query.metric}")
  private Boolean specificMetricsResponse;

  @Autowired
  private AccuWeatherForecastIntegrationApi accuWeatherForecastIntegrationApi;

  public List<AccuWeatherForecastDto> getForecastsForBaseCities(List<String> cities)
  {
    return accuWeatherForecastIntegrationApi
        .getForecastsForBaseCities(cities, apiKey, fullDetailsResponse, specificMetricsResponse);
  }

  public String getApiKey()
  {
    return apiKey;
  }

  public void setApiKey(String apiKey)
  {
    this.apiKey = apiKey;
  }

  public Boolean getFullDetailsResponse()
  {
    return fullDetailsResponse;
  }

  public void setFullDetailsResponse(Boolean fullDetailsResponse)
  {
    this.fullDetailsResponse = fullDetailsResponse;
  }

  public Boolean getSpecificMetricsResponse()
  {
    return specificMetricsResponse;
  }

  public void setSpecificMetricsResponse(Boolean specificMetricsResponse)
  {
    this.specificMetricsResponse = specificMetricsResponse;
  }

  public void setAccuWeatherForecastIntegrationApi(AccuWeatherForecastIntegrationApi accuWeatherForecastIntegrationApi)
  {
    this.accuWeatherForecastIntegrationApi = accuWeatherForecastIntegrationApi;
  }
}
