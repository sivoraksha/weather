package com.weather.application.service.impl;

import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.weather.application.dao.ForecastMongoRepository;
import com.weather.application.dao.ForecastRedisRepository;
import com.weather.application.dto.AccuWeatherForecastDto;
import com.weather.application.dto.MeteoprogForecastDto;
import com.weather.application.helper.mapping.AccuWeatherForecastMapper;
import com.weather.application.helper.mapping.MeteoprogForecastMapper;
import com.weather.application.model.AccuWeatherForecast;
import com.weather.application.model.MeteoprogForecast;
import com.weather.application.service.api.ForecastService;
import com.weather.application.service.api.MeteoprogForecastIntegrationApi;
import io.micrometer.core.annotation.Timed;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;

@Service
@PropertySource("classpath:service.properties")
@EnableAspectJAutoProxy
public class MainForecastService implements ForecastService
{
  Logger logger = LoggerFactory.getLogger(MainForecastService.class);

  @Value("#{'${meteoprog.cities.list.values}'.split(',')}")
  private List<String> meteoprogCitiesList;

  @Value("#{'${api.accu.weather.cities.list.values}'.split(',')}")
  private List<String> accuWeatherCitiesList;

  @Autowired
  private MeteoprogForecastMapper meteoprogForcastMapper;

  @Autowired
  private AccuWeatherForecastMapper accuWeatherForecastMapper;

  @Autowired
  private ForecastMongoRepository mongoRepository;

  @Autowired
  private ForecastRedisRepository redisRepository;

  @Autowired
  private MeteoprogForecastIntegrationApi meteoprogIntegrationApi;

  @Autowired
  private AccuWeatherApiService accuWeatherApiService;

  @Autowired
  private MeterRegistry registry;

  @Scheduled(cron = "0 * * ? * *")
  @Timed(value = "timerScheduledMainTest")
  public void executeForecastService()
  {
    logger.info("Scheduled task is EXECUTED");
    Counter counter = Counter
        .builder("main_service_task")
        .description("Indicates count of triggering scheduled task")
        .tags("dev", "counterTest")
        .register(registry);

    counter.increment(1.0);

    List<MeteoprogForecast> meteoprogForecasts = getForecastForBaseCitiesFromMeteoprogApi();
    //List<AccuWeatherForecast> accuWeatherForecasts = getForecastForBaseCitiesFromAccuWeatherApi();

    saveForecastsToMongoDB(meteoprogForecasts);
    //saveForecastsToRedisDB(accuWeatherForecasts);

    List<MeteoprogForecast> mongoData = mongoRepository.findAll();
    //List<AccuWeatherForecast> redisData = (List<AccuWeatherForecast>) redisRepository.findAll();
    logger.info("Scheduled task execution is FINISHED");
  }

  @Override
  public List<MeteoprogForecast> getForecastForBaseCitiesFromMeteoprogApi()
  {
    Timer timer = Timer.builder("timerMeteoprogGetForecasts").tag("meteoprog", "getForecast")
        .register(registry);
    Timer.Sample sample = Timer.start(registry);

    List<MeteoprogForecastDto> meteoprogForecasts = meteoprogIntegrationApi.
        getForecastsForBaseCities(meteoprogCitiesList);

    sample.stop(timer);
    return meteoprogForcastMapper.toMeteoprogForecastList(meteoprogForecasts);
  }

  @Override
  public List<AccuWeatherForecast> getForecastForBaseCitiesFromAccuWeatherApi()
  {
    Timer timer = Timer.builder("timerAccuWeatherGetForecasts").tag("accuWeather", "getForecast")
        .register(registry);
    Timer.Sample sample = Timer.start(registry);

    List<AccuWeatherForecastDto> accuWeatherForecasts = accuWeatherApiService
        .getForecastsForBaseCities(accuWeatherCitiesList);

    sample.stop(timer);
    return accuWeatherForecastMapper.toAccuWeatherForecastList(accuWeatherForecasts);
  }

  @Override
  public void saveForecastsToMongoDB(List<MeteoprogForecast> meteoprogForecasts)
  {
    if (CollectionUtils.isEmpty(meteoprogForecasts))
    {
      throw new RuntimeException("Empty response from Meteoprog forecast api.");
    }

    Timer timer = Timer.builder("timerSaveToMongo").tag("meteoprog", "saveForecasts")
        .register(registry);
    Timer.Sample sample = Timer.start(registry);

    meteoprogForecasts.forEach(mongoRepository::save);

    sample.stop(timer);
  }

  @Override
  public void saveForecastsToRedisDB(List<AccuWeatherForecast> forecasts)
  {
    if (CollectionUtils.isEmpty(forecasts))
    {
      throw new RuntimeException("Empty response from Meteoprog forecast api.");
    }

    Timer timer = Timer.builder("timerSaveToRedis").tag("redis", "saveForecasts")
        .register(registry);
    Timer.Sample sample = Timer.start(registry);

    forecasts.forEach(redisRepository::save);

    sample.stop(timer);
  }
}
