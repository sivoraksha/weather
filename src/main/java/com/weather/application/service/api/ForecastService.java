package com.weather.application.service.api;

import java.util.List;
import com.weather.application.model.AccuWeatherForecast;
import com.weather.application.model.MeteoprogForecast;

public interface ForecastService
{
  List<MeteoprogForecast> getForecastForBaseCitiesFromMeteoprogApi();

  List<AccuWeatherForecast> getForecastForBaseCitiesFromAccuWeatherApi();

  void saveForecastsToMongoDB(List<MeteoprogForecast> forecasts);

  void saveForecastsToRedisDB(List<AccuWeatherForecast> forecasts);
}
