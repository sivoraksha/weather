package com.weather.application.service.api;

import com.weather.application.dto.AccuWeatherForecastDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.stream.Collectors;

@Component
@FeignClient(name = "accuWeather", url = "https://dataservice.accuweather.com/forecasts/v1/daily/1day")
public interface AccuWeatherForecastIntegrationApi
{
    @RequestMapping(method = RequestMethod.GET, value = "/{city}")
    AccuWeatherForecastDto getForecast(@PathVariable("city") String city,
                                       @RequestParam("apikey") String apiKey,
                                       @RequestParam("details") boolean details,
                                       @RequestParam("metric") boolean metric);

    default List<AccuWeatherForecastDto> getForecastsForBaseCities(
            List<String> cities,
            String apiKey,
            boolean details,
            boolean metric)
    {
        return cities.stream()
                .map(city -> getForecast(city, apiKey, details, metric))
                .collect(Collectors.toList());
    }
}
