package com.weather.application.service.api;

import com.weather.application.dto.MeteoprogForecastDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.stream.Collectors;

@Component
@FeignClient(name = "meteoprog", url = "https://www.meteoprog.ua/ru/api/city")
public interface MeteoprogForecastIntegrationApi
{
    @RequestMapping(method = RequestMethod.GET, value = "/{city}",
            headers = "token=b9b3e3d337d48a25debfc42f36ba83dab19b7c89")
    MeteoprogForecastDto getForecast(@PathVariable("city") String city);

    default List<MeteoprogForecastDto> getForecastsForBaseCities(List<String> cities)
    {
        return cities.stream()
                .map(this::getForecast)
                .collect(Collectors.toList());
    }
}
