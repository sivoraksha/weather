package com.weather.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import com.mongodb.MongoClient;

@Configuration
@PropertySource("classpath:database.properties")
public class MongoDBConfiguration extends AbstractMongoConfiguration
{
  @Value("${mongodb.host}")
  private String mongoHost;

  @Value("${mongodb.port}")
  private String mongoPort;

  @Value("${mongodb.database}")
  private String mongoDB;

  @Override
  public MongoMappingContext mongoMappingContext() throws ClassNotFoundException
  {
    return super.mongoMappingContext();
  }

  @Override
  protected String getDatabaseName()
  {
    return mongoDB;
  }

  @Override
  public MongoClient mongoClient()
  {
    return new MongoClient(mongoHost + ":" + mongoPort);
  }
}
