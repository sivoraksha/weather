package com.weather.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

@Configuration
@PropertySource("classpath:database.properties")
public class RedisConfiguration extends CachingConfigurerSupport
{
  @Value("${redis.host}")
  private String hostname;

  @Value("${redis.port}")
  private int port;

  @Bean
  public JedisConnectionFactory jedisConnectionFactory()
  {
    JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();
    jedisConnectionFactory.setHostName(hostname);
    jedisConnectionFactory.setPort(port);
    return jedisConnectionFactory;
  }

  @Bean
  public RedisTemplate<Object, Object> redisTemplate()
  {
    RedisTemplate<Object, Object> redisTemplate = new RedisTemplate<>();
    redisTemplate.setConnectionFactory(jedisConnectionFactory());
    redisTemplate.setExposeConnection(true);
    return redisTemplate;
  }
}
